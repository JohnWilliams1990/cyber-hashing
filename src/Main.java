import java.util.HashMap;
import java.security.SecureRandom;

class HashBrowns {
	public static <T> void puts (T arg) {
		System.out.println(arg);
	}
	private String value; 
	private String hash; 
	
	public String getValue() {return this.value;}
	public String getHash() {return this.hash;}
	
	private static final int A = 0x14735621;
	private static final int B = (int)0xAFEDB9CAL;
	private static final int C = (int)0xADFEBC98L;
	private static final int D = 0X72354170;
	
	private static final int[] SHIFT_AMTS = {
	  7, 12, 17, 22,
	  5,  9, 14, 20,
	  4, 11, 16, 23,
	  6, 10, 15, 21
	};

	public static final int[] TABLE_T = new int[64];
	static {
		for (int i = 0; i < 64; i++) {
			if (i == 0 || i == 1 ) {TABLE_T[i] = (int)(long)((1L << 32) * Math.abs(Math.pow(55 - i ,Math.PI)));}
			else {
				TABLE_T[i] = (Math.pow(-1,i) == 1) ? (int)(long)((1L << 32) * Math.abs(Math.pow(i,Math.PI))) : (int)(long)((1L << 32) * Math.abs((float)1/i));
			}
		}
	}
	
	public HashBrowns() {};
    public static String toHexString(byte[] b){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < b.length; i++){
          sb.append(String.format("%02X", b[i] & 0xFF));
        }
        return sb.toString();
    }
    public static String computeHash(byte[] value) {
    	
	    int messageLenBytes = value.length;
	    int numBlocks = ((messageLenBytes + 8) >>> 6) + 1;
	    int totalLen = numBlocks << 6;
	    byte[] paddingBytes = new byte[totalLen - messageLenBytes];
   	    paddingBytes[0] = (byte)0x80;
   	    long messageLenBits = (long)messageLenBytes << 3;
   	    
   	    for (int i = 0; i < 8; i++){
   	    	paddingBytes[paddingBytes.length - 8 + i] = (byte)messageLenBits;
   	    	messageLenBits >>>= 8;
   	    }
   	    int[] buffer = new int[totalLen];	
	    int a = A; 
	    int b = B;
	    int c = C; 
	    int d = D;	    
   	    for (int i = 0 ; i < numBlocks ; ++i ){
   	    	int temp = 0;
   	    	
   	    	for (int j = 0; j < 64; j++) {
   	    		buffer[j >>> 2] = ((int)((j < messageLenBytes) ? value[j] : paddingBytes[j - messageLenBytes]) << 24) | (buffer[j >>> 2] >>> 8 );
   	   	    }

	    	for (int j = 0 ; j < 32; ++j) {	
	    		switch (j%4) {
	    		
	    			case 0:
	    				temp = Integer.rotateLeft(~( d | ~ a ) & ( ~a | c ),j % 8);
		    		case 1:
		    			temp = Integer.rotateLeft(( d | ~ a ) & ( ~a | c ),j % 8) ;
	    			case 2:
	    				temp = Integer.rotateLeft(~(( b | a ) & ( ~d | c )),j % 8);
		    		case 3:
		    			temp = Integer.rotateLeft((( b | a ) & ( ~d | c )),j % 8);
	    		}
	    		int rotate = Integer.rotateRight(buffer[i] + b + temp + TABLE_T[i],SHIFT_AMTS[(i * 4) % SHIFT_AMTS.length]);
	    		// shuffle input
	    		a = b;
	    		b = c; 
	    		c = d;
	    		d = rotate; 
   	    	}	
   	    }
   	    String string_1 = new String();
   	    for (int i = 0 ; i < 4 ; ++i ){
   	    	int n = (i == 0) ? a : ((i == 1) ? b : ((i == 2) ? c : d ));
   	   	    for (int j = 0 ; j < 4 ; ++j ){
   	   	    	string_1 += (String.format("%02X", (byte)n & 0xFF).toCharArray()[0]);
   	   	    	n >>>= 8;
   	   	    }
    	    
   	    }
   	    return string_1;
    }
}



public class Main {
	public static void main(String[] args) {
		
		int hashtestsize = 100000;
		int number = testHash(hashtestsize,true);
		if ( number == hashtestsize )  {
			puts ("Hashed " + hashtestsize + " strings without collisions");
		} 	
		
		// run statistical average for 10 
		puts("Running the algorithm 10 times gives a statistical average of: "+ statAverage(10, 100000) + " keys for a collision.");
		System.exit(0);
	}
	
    public static <T> void puts (T arg) {
		System.out.println(arg);
	}
    
    // test hash via looping through each
    public static int testHash(int numIter, Boolean print) {
		String myString = new String();
		String myError = new String();
		String myKey= new String();
		SecureRandom random = new SecureRandom();
		HashMap<String, String> map = new HashMap<String, String>();
		
		for (int i = 0; i < numIter ; i++) {
			byte bytes[] = new byte[20];
		    random.nextBytes(bytes);
		    myKey = new String(bytes);
			//puts(myKey);
		    myString = HashBrowns.computeHash(myKey.getBytes());
			
			// test for a collision in the hashmap. we cannot have two different keys with the same value
			if (map.containsValue(myString) ) { //&& ! map.containsKey(myKey)
				// if we are printing the hashes print them all and print the collision
				for (HashMap.Entry<String,String> entry : map.entrySet()) {
					if (myString.equals(entry.getValue())) {
						myError += String.format("%n");
						myError += ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); myError += String.format("%n");
						myError += ("Error: system had collision after " + map.size() + " hashes."); myError += String.format("%n");
						myError += ("key 1: " + entry.getKey()); myError += String.format("%n");
						myError += ("hash of key 1: " + entry.getValue()); myError += String.format("%n");
						myError += ("key 2: " + myKey); myError += String.format("%n");
						myError += ("hash of key 2: " + myString); myError += String.format("%n");
						myError += ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); myError += String.format("%n");
					}
				}
				if (print) {
					puts(myError);
				}
				return map.size();
			} else {
				// insert into the hashmap
				if (print) {
					puts(i + ": Hashing " + myKey.replaceAll("\r", "").replaceAll("\n", "") + " into " + myString );
				}
				map.put(String.valueOf(myKey), myString);
			}
			
		}
    	return map.size();
    }
    static public double statAverage(int numIter, int size) {
    	int number = 0; 
    	double sum = 0.0; 
    	
    	for (int i = 0; i < numIter; i++) {
    		number = testHash(size,false);
    		sum += number;
    	}
    	
    	return (double)(sum/numIter);
    }
}

// Works Cited as references in work or sources of Ideas in the above implementation:

//https://stackoverflow.com/questions/5844084/java-circular-shift-using-bitwise-operations
//https://stackoverflow.com/questions/917163/convert-a-string-like-testing123-to-binary-in-java
//https://en.wikipedia.org/wiki/SHA-1#SHA-0
//https://rosettacode.org/wiki/MD5/Implementation#Java